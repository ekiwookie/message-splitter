import re

DISPLAY_PART_LENGTH = 40

block_tag_pattern = re.compile("(<\/?(?:p|b|strong|i|ul|ol|div|span).*?>)")
tag_name_patter = re.compile("<\/?(\w*).*>")


class SplitterException(Exception): ...


class BlockTagStack:
    def __init__(self):
        self._stack = list()
        self._len = 0

    def _get_element_len(self, element):
        element_len = len(element) + len(self._get_tag_name(element)) + 3
        return element_len

    def _get_tag_name(self, element):
        return tag_name_patter.search(element).groups()[0]

    def get_len_with_element(self, element):
        return self._len + self._get_element_len(element)

    def append(self, element):
        self._stack.append(element)
        self._len += self._get_element_len(element)

    def pop(self):
        if not self._stack:
            return
        element = self._stack.pop()
        self._len -= self._get_element_len(element)
        return element

    def get_close_tags(self):
        close_tags = tuple(
            map(lambda item: f"</{self._get_tag_name(item)}>", self._stack)
        )
        return "".join(list(reversed(close_tags)))

    def get_open_tags(self):
        return "".join(self._stack)

    def __len__(self):
        return self._len


class PartHandler:
    def __init__(self, max_len, block_tag_stack):
        self._block_tag_stack = block_tag_stack
        self._max_len = max_len

    def _is_tag(self, text):
        _is_tag = bool(block_tag_pattern.match(text))
        if not _is_tag:
            return False, None
        return True, not text.startswith("</")

    def handle_part(self, part, message):
        is_tag, is_openning = self._is_tag(part)
        if len(part) > self._max_len:
            raise SplitterException(
                f"part {part[:DISPLAY_PART_LENGTH]} length more than max-len"
            )
        elif is_tag and is_openning:
            if self._block_tag_stack.get_len_with_element(part) <= self._max_len:
                self._block_tag_stack.append(part)
                return True
            raise SplitterException(
                f"tag {part[:DISPLAY_PART_LENGTH]} with close tag length more than max-len in {self._block_tag_stack.get_open_tags()}"
            )
        elif is_tag and not is_openning:
            self._block_tag_stack.pop()
            return True
        elif len(message + part) <= self._max_len:
            return True
        return False


class MessageSplitter:
    def __init__(self, source, max_len, _PartHandler=PartHandler):
        self._source = block_tag_pattern.split(source)
        self._max_len = max_len
        self._block_tag_stack = BlockTagStack()
        self._part_handler = _PartHandler(max_len, self._block_tag_stack)
        self._i = 0
        self._message = ""

    def get_message(self):
        self._message = self._block_tag_stack.get_open_tags()
        if self._i >= len(self._source):
            return False
        for i in range(self._i, len(self._source)):
            part = self._source[i]
            if not self._part_handler.handle_part(part, self._message):
                return self._get_message()
            else:
                self._i = i + 1
                self._message += part
            if i == len(self._source) - 1:
                self._i = i + 1
                return self._get_message()

    def _get_message(self):
        return self._message + self._block_tag_stack.get_close_tags()


def split_message(source: str, max_len):
    message_splitter = MessageSplitter(source, max_len)
    while True:
        message = message_splitter.get_message()
        if message:
            yield message
        else:
            return
