# HTML Splitter

This Python program is designed to split HTML files into segments, ensuring that tags are properly closed where necessary.

## Usage

```bash
python main.py [--max-len <int>] <input_file.html>
```
