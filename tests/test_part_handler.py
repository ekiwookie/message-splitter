import unittest

from split_message import BlockTagStack, PartHandler, SplitterException


class PartHandlerTestCase(unittest.TestCase):
    def test_is_tag(self):
        block_tag_stack = BlockTagStack()
        part_handler = PartHandler(15, block_tag_stack)
        self.assertEqual(part_handler._is_tag("<ul>"), (True, True))
        self.assertEqual(part_handler._is_tag("</ul>"), (True, False))
        self.assertEqual(part_handler._is_tag("<html>"), (False, None))

    def test_handle_big_part(self):
        block_tag_stack = BlockTagStack()
        part_handler = PartHandler(5, block_tag_stack)
        self.assertRaises(SplitterException, part_handler.handle_part, "testtest", "")

    def test_handle_opening(self):
        block_tag_stack = BlockTagStack()
        part_handler = PartHandler(12, block_tag_stack)
        part_handler.handle_part("<div>", "")
        self.assertEqual(block_tag_stack.pop(), "<div>")
        part_handler.handle_part("<div>", "")
        self.assertRaises(SplitterException, part_handler.handle_part, "<div>", "")

    def test_handle_closing(self):
        block_tag_stack = BlockTagStack()
        part_handler = PartHandler(50, block_tag_stack)
        part_handler.handle_part("<div>", "")
        part_handler.handle_part("<ul>", "")
        part_handler.handle_part("</ul>", "")
        self.assertEqual(block_tag_stack.pop(), "<div>")

    def test_handle_not_tag(self):
        block_tag_stack = BlockTagStack()
        part_handler = PartHandler(4, block_tag_stack)
        self.assertEqual(part_handler.handle_part("test", ""), True)
        self.assertEqual(part_handler.handle_part("test", "a"), False)
