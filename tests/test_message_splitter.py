import unittest

from split_message import MessageSplitter, split_message


class PartHandlerMockTrue:
    def __init__(self, *args, **kwargs): ...

    def handle_part(self, part, message):
        return True


class PartHandlerMockFalse:
    def __init__(self, *args, **kwargs): ...

    def handle_part(self, part, message):
        return False


class PartHandlerTestCase(unittest.TestCase):
    def test_exit(self):
        message_splitter = MessageSplitter(
            "<div><span>test</span></div>", 20, PartHandlerMockTrue
        )
        message_splitter._i = 9
        self.assertEqual(message_splitter.get_message(), False)

    def test_get_message(self):
        source = "<div><span>test</span></div>"
        message_splitter = MessageSplitter(source, 20, PartHandlerMockTrue)
        self.assertEqual(message_splitter.get_message(), source)
        message_splitter = MessageSplitter(source, 20, PartHandlerMockFalse)
        self.assertEqual(message_splitter.get_message(), "")
