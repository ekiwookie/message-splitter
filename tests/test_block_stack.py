import unittest

from split_message import BlockTagStack


class BlockStackTestCase(unittest.TestCase):
    def test_append(self):
        block_tag_stack = BlockTagStack()
        self.assertEqual(len(block_tag_stack._stack), 0)
        block_tag_stack.append("<ul>")
        self.assertEqual(len(block_tag_stack._stack), 1)
        self.assertEqual(len(block_tag_stack), 9)

    def test_pop(self):
        block_tag_stack = BlockTagStack()
        self.assertEqual(block_tag_stack.pop(), None)
        block_tag_stack.append("<ul>")
        block_tag_stack.append("<div>")
        self.assertEqual(block_tag_stack.pop(), "<div>")

    def test_len(self):
        block_tag_stack = BlockTagStack()
        self.assertEqual(block_tag_stack.get_len_with_element("<ul>"), 9)
        block_tag_stack.append("<ul>")
        self.assertEqual(block_tag_stack.get_len_with_element('<div class="test">'), 33)

    def test_tags(self):
        block_tag_stack = BlockTagStack()
        block_tag_stack.append("<div>")
        block_tag_stack.append("<ul class='test'>")
        self.assertEqual(block_tag_stack.get_close_tags(), "</ul></div>")
        self.assertEqual(block_tag_stack.get_open_tags(), "<div><ul class='test'>")
