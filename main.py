from argparse import ArgumentParser
from pathlib import Path

from split_message import split_message


def _parse_agrs(args: list[str] | None = None) -> ArgumentParser:
    parser = ArgumentParser(
        description="",
        prog="Message splitter",
    )
    parser.add_argument(
        "--max-len",
        type=int,
        dest="max_length",
        help="Max length of message",
        nargs="?",
        default=3072,
    )
    parser.add_argument(dest="path", type=lambda p: Path(p).absolute())
    return parser.parse_args(args)


def main(
    args: list[str] | None = None,
):
    args = _parse_agrs(args)
    with open(args.path, "r") as file:
        source = file.read().rstrip()
    for i, msg in enumerate(split_message(source, args.max_length)):
        print(f"-- fragment #{i+1}: {len(msg)} chars --")
        print(msg)


if __name__ == "__main__":
    main()
